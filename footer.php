    </div>
    <footer id="footer">
        <div class="first-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="subtitle">
                            <h4 class="text-uppercase">Mapa de Navegação</h4>
                        </div>

                        <ul class="list-unstyled">
                            <li>Conheça</li>
                            <li>Planos</li>
                            <li>Blog</li>
                            <li>Sou cliente</li>
                            <li>Cadastre-se</li>
                            <li>Contato</li>
                        </ul>
                    </div>
                    <div class="col-lg-5">
                        <div class="about">
                            <div class="subtitle">
                                <h4 class="text-uppercase">Sobre nós</h4>
                            </div>

                            <p class="text-justify">O EleveCRM foi concebido, objetivando acelerar o processo comercial, integrar práticas e formalizar etapas de  atendimento, permitindo formatar e disponibilizar documentos, formulários e e-mails padrões, e acompanhar todo o ciclo comercial, com leveza, simplicidade e muito gerenciamento.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="notices">
                            <div class="subtitle">
                                <h4 class="text-uppercase">Últimas do blog</h4>
                            </div>

                            <div class="part-section">
                                <a href="#">O que seria Customer Success?</a>
                                <h6>12/07/2017</h6>
                            </div>

                            <div class="part-section">
                                <a href="#">A boa entrega é a garantia da próxima venda!</a>
                                <h6>12/07/2017</h6>
                            </div>

                            <div class="part-section">
                                <a href="#">Afinal o que são vendas complexas?</a>
                                <h6>12/07/2017</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="last-content">
            <div class="container">
                <div class="thumb-bottom">
                    <img class="img-responsive center-block" src="assets/images/logos/elevecrm-inverse.png" title="EleveCRM" alt="EleveCRM">
                </div>
            </div>
        </div>
        <script rel="javascript" type="text/javascript" src="assets/js/bootstrap/bootstrap.min.js"></script>
        <script rel="javascript" type="text/javascript" src="assets/js/main.js"></script>
    </footer>
</body>
</html>