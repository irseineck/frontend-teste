<div class="form-contact">
    <form action="" method="POST">
        <h4><strong>Dados de acesso</strong></h4>

        <div class="form-group">
            <label for="nome" class="control-label">Nome</label>
            <input type="text" name="nome" class="form-control">
        </div>

        <div class="form-group">
            <label for="email" class="control-label">E-mail</label>
            <input type="email" name="email" class="form-control">
        </div>

        <div class="form-group">
            <label for="senha" class="control-label">Senha</label>
            <input type="password" name="senha" class="form-control">
        </div>

        <div class="form-group">
            <label for="senha_confirm" class="control-label">Confirmação de senha</label>
            <input type="text" name="senha_confirm" class="form-control">
        </div>

        <h4><strong>Dados da empresa</strong></h4>

        <div class="form-group">
            <label for="cnpj" class="control-label">CNPJ</label>
            <input type="number" name="cnpj" class="form-control">
        </div>

        <div class="form-group">
            <label for="razao_social" class="control-label">Razão Social</label>
            <input type="text" name="razao_social" class="form-control">
        </div>

        <div class="form-group">
            <label for="nome_fantasia" class="control-label">Nome fantasia</label>
            <input type="text" name="nome_fantasia" class="form-control">
        </div>

        <div class="form-group">
            <label for="site" class="control-label">Site</label>
            <input type="text" name="site" class="form-control">
        </div>

        <div class="form-group">
            <label for="telefone" class="control-label">Telefone</label>
            <input type="number" name="telefone" class="form-control">
        </div>

        <div class="last-form">
            <div class="row form-sections">
                <div class="col-lg-6 col-md-6">
                    <div class="row">
                        <label class="terms">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                <input type="checkbox">
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                <p>Concordo com os <strong>termos de serviço</strong></p>
                            </div>
                        </label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <button type="submit" class="btn btn-lg btn-block text-uppercase">Criar conta</button>
                </div>
            </div>
        </div>
    </form>
</div>