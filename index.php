<?php 
require_once 'config.php';
require_once 'header.php'; 
require_once 'header-navbar.php' ?>
    <main role="main">
        <header class="header-info">
            <div class="container">
                <h1 class="text-center">Ótima escolha!</h1>

                <p class="text-center">Crie  sua conta <strong>gratuita</strong> e amplie seus resultados.</p>
                <p class="text-center">EleveCRM é a forma mais eficaz de elevar os resultados de seu time comercial!</p>
            </div>
        </header>

        <section class="main-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 hidden-xs">
                        <div class="main-info">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="thumb-main">
                                        <img class="img-responsive center-block" src="assets/images/notebook.png" title="Eleve seu negócio" alt="Eleve seu negócio">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="last-info">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="main-message">
                                        <div class="content">
                                            <p>Você está se cadastrandoe em nossa versão beta, totalmente gratuita por tempo indeterminado. Aproveite e veja como é facil gerenciar suas vendas e seu time comercial usando o Eleve CRM.</p>
                                            <div class="hr-colored">
                                                <hr>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-lg-offset-1">
                        <?php require_once 'form-contact.php' ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php require_once 'footer.php';