module.exports = function(grunt) {

    var config = {

        pkg: grunt.file.readJSON( 'package.json' ),

        dirs: {
            css: '../assets/css',
            js: '../assets/js',
            libs: '../assets/js/libs',
            sass: '../assets/sass',
            images: '../assets/images',
            fonts: '../assets/fonts'
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed',
                    sourcemap: 'none'
                },
                files: [{
                    expand: true,
                    cwd: '<%= dirs.sass %>',
                    src: ['*.scss'],
                    dest: '<%= dirs.css %>',
                    ext: '.css'
                }]
            }
        },

        js: {
            dist: {
                options: {
                    style: 'compressed',
                    sourcemap: 'none'
                },
                files: [{
                    expand: true,
                    cwd: '<%= dirs.js %>',
                    src: ['*.js'],
                    dest: '<%= dirs.libs %>',
                    ext: '.js'
                }]
            }
        },

        watch: {
            sass: {
                files: [
                    '<%= dirs.sass %>/**'
                ],
                tasks: ['sass']
            },
            js: {
                files: [
                    '<%= dirs.js %>/**'
                ],
                tasks: ['js']
            },
            livereload: {
                options: {
                    livereload: true
                },
                files: [
                    '<%= dirs.css %>/*.css',
                    '<%= dirs.js %>/*.js',
                    '../**/*.php'
                ]
            },
            options: {
                spawn: false
            }
        }
    }

    grunt.initConfig( config );

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-notify');

    grunt.registerTask('default', ['watch']);
};